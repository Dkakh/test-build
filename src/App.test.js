import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn ci', () => {
  render(<App />);
  const title = screen.getByText(/learn CI/i);
  expect(title).toBeInTheDocument();
});

test('renders button', () => {
  render(<App />);
  const incrementButton = screen.getByText(/\+2/i);
  expect(incrementButton).toBeInTheDocument();
});
