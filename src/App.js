import React from 'react';
import './App.css';

function App() {
  const [number, setNumber] = React.useState(0);
  const [result, setResult] = React.useState();

  const incrementByTwo = (num) => {
    setResult(() => Number(num) + 2);
  };

  return (
    <div className='App'>
      <header>
        <p>Learn CI</p>
        <p>Hello</p>
      </header>
      <main>
        <input type='number' value={number} onChange={(evt) => setNumber(evt.target.value)} />
        <div className='button' onClick={() => incrementByTwo(number)}>
          +2
        </div>
        <div className='button' onClick={() => setResult()}>
          reset
        </div>
        <div className='result'> Result: {result}</div>
      </main>
    </div>
  );
}

export default App;
